package beewhy.yanina.yumstock.stock.stocklist

import beewhy.yanina.yumstock.stock.data.StockRepository
import beewhy.yanina.yumstock.stock.data.model.StockTicker
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.observers.TestObserver
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString


class StockListViewModelTest {
    private val stockRepo: StockRepository = mock()
    private val filterGenerator: FilterGenerator = mock()

    private val stockListViewModel = StockListViewModel(filterGenerator, stockRepo)

    @Test
    fun onGetUiState_successfulRespNoFilters_returnsFullList() {
        whenever(stockRepo.getStockTickers()).thenReturn(Observable.just(fakeStockTickers()))
        whenever(filterGenerator.validateCompanyFilter(anyString(), any())).thenReturn(true)
        whenever(filterGenerator.validateNameFilter(anyString(), any())).thenReturn(true)

        val testObserver = TestObserver<StockListState>()
        stockListViewModel.getUiState().subscribe(testObserver)
        testObserver.assertValueAt(0, StockListState.Loading)
        testObserver.assertValueAt(
            1,
            StockListState.StockTickers(fakeStockTickers(), setOf(), setOf())
        )
    }

    @Test
    fun onGetUiState_successfulRespNameFilterApplied_returnsFilteredList() {
        whenever(stockRepo.getStockTickers()).thenReturn(Observable.just(fakeStockTickers()))
        whenever(filterGenerator.validateCompanyFilter(anyString(), any())).thenReturn(true)
        whenever(filterGenerator.validateNameFilter(anyString(), any())).thenReturn(true)

        stockListViewModel.submitIntent(StockListIntent.FilterByName("Apple Inc"))
        val testObserver = TestObserver<StockListState>()
        stockListViewModel.getUiState().subscribe(testObserver)
        testObserver.assertValueAt(0, StockListState.Loading)
        testObserver.assertValueAt(
            1,
            StockListState.StockTickers(
                listOf(
                    StockTicker(
                        "AAPL",
                        "Apple Inc",
                        276.54,
                        listOf("Tech")
                    )
                ), setOf(), setOf()
            )
        )
    }

    @Test
    fun onGetUiState_successfulRespCompanyFilterApplied_returnsFilteredList() {
        whenever(stockRepo.getStockTickers()).thenReturn(Observable.just(fakeStockTickers()))
        whenever(filterGenerator.validateCompanyFilter(anyString(), any())).thenReturn(true)
        whenever(filterGenerator.validateNameFilter(anyString(), any())).thenReturn(true)

        stockListViewModel.submitIntent(StockListIntent.FilterByCompany("Auto"))
        val testObserver = TestObserver<StockListState>()
        stockListViewModel.getUiState().subscribe(testObserver)
        testObserver.assertValueAt(0, StockListState.Loading)
        testObserver.assertValueAt(
            1,
            StockListState.StockTickers(
                listOf(
                    StockTicker(
                        "F",
                        "Ford Motor Company",
                        6.11,
                        listOf("Auto")
                    )
                ), setOf(), setOf()
            )
        )
    }

    private fun fakeStockTickers() = listOf<StockTicker>(
        StockTicker("AAPL", "Apple Inc", 276.54, listOf("Tech")),
        StockTicker("Yum", "Yum! Brands Inc", 101.22, listOf("Food", "Tech")),
        StockTicker("F", "Ford Motor Company", 6.11, listOf("Auto"))
    )
}