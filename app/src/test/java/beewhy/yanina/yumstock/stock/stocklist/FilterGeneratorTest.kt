package beewhy.yanina.yumstock.stock.stocklist

import beewhy.yanina.yumstock.stock.data.model.StockTicker
import org.junit.Assert.*
import org.junit.Test

class FilterGeneratorTest {
    private val filterGenerator = FilterGenerator()

    @Test
    fun onGenerateNameFilters_returnsSetOfUniquePrefixes() {
        val filterSet = filterGenerator.generateNameFilters(fakeStockTickers())
        assertTrue(filterSet.isNotEmpty())
        assertTrue(filterSet.contains("Ap"))
        assertTrue(filterSet.contains("Yu"))
        assertTrue(filterSet.contains("Fo"))
    }

    @Test
    fun onGenerateCompanyFilters_returnsSetOfUniqueCompanyTypes() {
        val filterSet = filterGenerator.generateCompanyFilters(fakeStockTickers())
        assertTrue(filterSet.isNotEmpty())
        assertTrue(filterSet.contains("Tech"))
        assertTrue(filterSet.contains("Food"))
        assertTrue(filterSet.contains("Auto"))
    }

    @Test
    fun onValidateNameFilter_filterValid_returnsTrue() {
        val valid = filterGenerator.validateNameFilter("Yu", fakeStockTickers())
        assertTrue(valid)
    }

    @Test
    fun onValidateNameFilter_emptyFilter_returnsFalse() {
        val valid = filterGenerator.validateNameFilter("", fakeStockTickers())
        assertFalse(valid)
    }

    @Test
    fun onValidateNameFilter_filterInvalid_returnsFalso() {
        val valid = filterGenerator.validateNameFilter("Zu", fakeStockTickers())
        assertFalse(valid)
    }

    @Test
    fun onValidateCompanyFilter_filterValid_returnsTrue() {
        val valid = filterGenerator.validateCompanyFilter("Auto", fakeStockTickers())
        assertTrue(valid)
    }

    @Test
    fun onValidateCompanyFilter_emptyFilter_returnsFalse() {
        val valid = filterGenerator.validateCompanyFilter("", fakeStockTickers())
        assertFalse(valid)
    }

    @Test
    fun onValidateCompanyFilter_filterInvalid_returnsFalse() {
        val valid = filterGenerator.validateCompanyFilter("Zu", fakeStockTickers())
        assertFalse(valid)
    }

    private fun fakeStockTickers() = listOf<StockTicker>(
        StockTicker("AAPL", "Apple Inc", 276.54, listOf("Tech")),
        StockTicker("Yum", "Yum! Brands Inc", 101.22, listOf("Food", "Tech")),
        StockTicker("F", "Ford Motor Company", 6.11, listOf("Auto"))
    )
}