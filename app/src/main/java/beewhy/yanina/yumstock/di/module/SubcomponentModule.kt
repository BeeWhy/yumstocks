package beewhy.yanina.yumstock.di.module

import beewhy.yanina.yumstock.di.StockComponent
import dagger.Module

@Module(subcomponents = [StockComponent::class])
class SubcomponentModule {}