package beewhy.yanina.yumstock.di

import beewhy.yanina.yumstock.stock.StockActivity
import beewhy.yanina.yumstock.stock.stockdetail.StockDetailFragment
import beewhy.yanina.yumstock.stock.stocklist.StockListFragment
import dagger.Subcomponent

@ActivityScope
@Subcomponent
interface StockComponent {

    @Subcomponent.Factory
    interface Factory {
        fun create(): StockComponent
    }

    fun inject(activity: StockActivity)
    fun inject(stockListFragment: StockListFragment)
    fun inject(stockDetailFragment: StockDetailFragment)
}