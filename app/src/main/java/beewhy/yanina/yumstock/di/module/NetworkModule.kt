package beewhy.yanina.yumstock.di.module

import beewhy.yanina.yumstock.BuildConfig
import beewhy.yanina.yumstock.YumApplication
import beewhy.yanina.yumstock.stock.data.datasource.network.SocketService
import beewhy.yanina.yumstock.stock.data.datasource.network.StockDetailService
import beewhy.yanina.yumstock.stock.data.datasource.network.streamadapter.RxJava3StreamAdapterFactory
import com.tinder.scarlet.Lifecycle
import com.tinder.scarlet.Scarlet
import com.tinder.scarlet.lifecycle.android.AndroidLifecycle
import com.tinder.scarlet.messageadapter.moshi.MoshiMessageAdapter
import com.tinder.scarlet.retry.ExponentialWithJitterBackoffStrategy
import com.tinder.scarlet.websocket.okhttp.newWebSocketFactory
import dagger.Module
import dagger.Provides
import hu.akarnokd.rxjava3.retrofit.RxJava3CallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class NetworkModule {

    @Singleton
    @Provides
    fun provideOkHttpClient(): OkHttpClient = OkHttpClient.Builder()
        .connectTimeout(10, TimeUnit.SECONDS)
        .readTimeout(10, TimeUnit.SECONDS)
        .writeTimeout(10, TimeUnit.SECONDS)
        .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC))
        .build()

    @Singleton
    @Provides
    fun provideRetrofit(): Retrofit =
        Retrofit.Builder().baseUrl(BuildConfig.SERVER_URL)
            .addConverterFactory(MoshiConverterFactory.create())
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .build()

    @Singleton
    @Provides
    fun provideStockDetailService(retrofit: Retrofit): StockDetailService =
        retrofit.create(StockDetailService::class.java)

    @Singleton
    @Provides
    fun provideLifecycle(app: YumApplication) = AndroidLifecycle.ofApplicationForeground(app)

    @Provides
    fun provideBackoffStrategy() = ExponentialWithJitterBackoffStrategy(5000, 5000)

    @Singleton
    @Provides
    fun provideScarlet(
        okHttpClient: OkHttpClient,
        backoffStrategy: ExponentialWithJitterBackoffStrategy,
        lifecycle: Lifecycle
    ) = Scarlet.Builder()
        .webSocketFactory(okHttpClient.newWebSocketFactory(BuildConfig.SOCKET_URL))
        .addMessageAdapterFactory(MoshiMessageAdapter.Factory())
        .addStreamAdapterFactory(RxJava3StreamAdapterFactory())
        .backoffStrategy(backoffStrategy)
        .lifecycle(lifecycle)
        .build()

    @Singleton
    @Provides
    fun provideWebSocket(scarlet: Scarlet) = scarlet.create(SocketService::class.java)
}