package beewhy.yanina.yumstock.di.module

import android.app.Application
import beewhy.yanina.yumstock.YumApplication
import dagger.Binds
import dagger.Module

@Module
abstract class AppModule {

    @Binds
    abstract fun provideContext(app: YumApplication): Application
}