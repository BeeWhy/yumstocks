package beewhy.yanina.yumstock.di

import beewhy.yanina.yumstock.YumApplication
import beewhy.yanina.yumstock.di.module.AppModule
import beewhy.yanina.yumstock.di.module.NetworkModule
import beewhy.yanina.yumstock.di.module.SubcomponentModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(modules = [SubcomponentModule::class, AppModule::class, NetworkModule::class])
interface ApplicationComponent : AndroidInjector<YumApplication> {

    //    fun inject(app: YumApplication)
    @Component.Factory
    interface Factory : AndroidInjector.Factory<YumApplication> {
        override fun create(@BindsInstance instance: YumApplication): ApplicationComponent
    }

    fun stockComponent(): StockComponent.Factory
//    fun inject(activity: StockActivity)
}