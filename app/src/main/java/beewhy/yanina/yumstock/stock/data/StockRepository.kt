package beewhy.yanina.yumstock.stock.data

import android.util.Log
import beewhy.yanina.yumstock.stock.data.datasource.network.SocketService
import beewhy.yanina.yumstock.stock.data.datasource.network.StockDetailService
import beewhy.yanina.yumstock.stock.data.model.StockDetail
import beewhy.yanina.yumstock.stock.data.model.StockTicker
import com.tinder.scarlet.WebSocket
import io.reactivex.rxjava3.core.Observable
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class StockRepository @Inject constructor(
    private val socketService: SocketService,
    private val stockDetailService: StockDetailService
) {
    fun getStockTickers(): Observable<List<StockTicker>> = observeWebSocketEvent()
        .doOnNext {
            Log.d("StockRepo", "Type of event $it")
            if (it is WebSocket.Event.OnConnectionFailed) {
                Observable.error<WebSocket.Event>(Throwable("Bummer!"))
            }
        }
        .filter { it is WebSocket.Event.OnConnectionOpened<*> }
        .switchMap {
            socketService.observeTicker()
        }
        .debounce(100, TimeUnit.MILLISECONDS)
        .toObservable()

    private fun observeWebSocketEvent() = socketService.observeWebSocketEvent()

    fun getStockDetail(id: String) = stockDetailService.getStockDetailFor(id)

    private fun fakeStockTickers() = listOf<StockTicker>(
        StockTicker("AAPL", "Apple Inc", 276.54, listOf("Tech")),
        StockTicker("Yum", "Yum! Brands Inc", 101.22, listOf("Food", "Tech")),
        StockTicker("F", "Ford Motor Company", 6.11, listOf("Auto"))
    )

    private fun fakeStockDetail() =
        StockDetail("AAPL", "Apple Inc", 276.54, listOf("Tech"), 105.44, "Gamarnika 4-109", "", "")
}