package beewhy.yanina.yumstock.stock.data.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class StockDetail(
    val id: String,
    val name: String,
    val price: Double,
    val companyType: List<String>,
    val allTimeHigh: Double,
    val address: String,
    val imageUrl: String,
    val website: String
)