package beewhy.yanina.yumstock.stock.stockdetail

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import beewhy.yanina.yumstock.databinding.FragmentStockDetailsBinding
import beewhy.yanina.yumstock.stock.StockActivity
import beewhy.yanina.yumstock.stock.data.model.StockDetail
import com.google.android.material.snackbar.Snackbar
import io.reactivex.rxjava3.disposables.CompositeDisposable
import javax.inject.Inject

class StockDetailFragment : Fragment() {
    private var binding: FragmentStockDetailsBinding? = null

    private val args: StockDetailFragmentArgs by navArgs()

    private val compositeDisposable by lazy { CompositeDisposable() }

    @Inject
    lateinit var viewModel: StockDetailViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentStockDetailsBinding.inflate(inflater)
        subscribeUi()
        return binding?.root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (activity as StockActivity).stockComponent.inject(this)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        compositeDisposable.clear()
        binding = null
    }

    override fun onDestroy() {
        compositeDisposable.dispose()
        super.onDestroy()
    }

    private fun subscribeUi() {
        compositeDisposable.add(viewModel.getUiState(args.stockId).subscribe({
            when (it) {
                is StockDetailState.Loading -> showProgressBar()
                is StockDetailState.StockDetailReady -> populateUi(it.stockDetail)
                is StockDetailState.Error -> showError(it.error ?: "Unexpected error!")
            }
        }, {
            hideProgressBar()
            showError(it.message ?: "Unexpected error!")
            Log.d("Bummer!", it.message)
        }))
    }

    private fun populateUi(stockdDetail: StockDetail) {
        binding?.let {
            with(it) {
                textTickerId.text = stockdDetail.id
                textTickerName.text = stockdDetail.name
                textTickerCompanyType.text =
                    " ${stockdDetail.companyType.joinToString(separator = " ")}"
                textTickerPrice.text = "USD ${stockdDetail.price}"
                textAllTimeHigh.text = "USD ${stockdDetail.allTimeHigh}"
                textAddress.text = stockdDetail.address
                textWebsite.text = stockdDetail.website
                //todo image
            }
        }
    }

    private fun showError(error: String) {
        binding?.let {
            Snackbar.make(it.layoutStockDetails, error, Snackbar.LENGTH_LONG)
                .show()
        }
    }

    private fun showProgressBar() {
        binding?.progressbar?.visibility = View.VISIBLE
    }

    private fun hideProgressBar() {
        binding?.progressbar?.visibility = View.GONE
    }
}