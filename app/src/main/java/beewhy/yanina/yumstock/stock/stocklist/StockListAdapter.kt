package beewhy.yanina.yumstock.stock.stocklist

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import beewhy.yanina.yumstock.R
import beewhy.yanina.yumstock.databinding.ItemStockTickerBinding
import beewhy.yanina.yumstock.stock.data.model.StockTicker
import javax.inject.Inject

class StockListAdapter @Inject constructor() :
    ListAdapter<StockTicker, StockListAdapter.StockViewHolder>(StockTickerDiffCallback()) {

    inner class StockViewHolder(internal val binding: ItemStockTickerBinding) :
        RecyclerView.ViewHolder(binding.root) {
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StockViewHolder {
        val binding =
            ItemStockTickerBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return StockViewHolder(binding)
    }

    override fun onBindViewHolder(holder: StockViewHolder, position: Int) {
        with(holder) {
            with(getItem(position)) {
                binding.textTickerId.text = this.id
                binding.textTickerName.text = this.name
                binding.textTickerPrice.text = "USD ${this.price}"
                binding.textTickerCompanyType.text =
                    " ${this.companyType.joinToString(separator = " ",
                        transform = { it.capitalize() })}"

                itemView.setOnClickListener {
                    val directions =
                        StockListFragmentDirections.actionStockListFragmentToStockDetailFragment(
                            this.id
                        )
                    it.findNavController().navigate(directions)
                }
            }
        }
    }
}

private class StockTickerDiffCallback : DiffUtil.ItemCallback<StockTicker>() {

    override fun areItemsTheSame(oldItem: StockTicker, newItem: StockTicker): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: StockTicker, newItem: StockTicker): Boolean {
        return oldItem == newItem
    }
}