package beewhy.yanina.yumstock.stock

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import beewhy.yanina.yumstock.YumApplication
import beewhy.yanina.yumstock.databinding.ActivityStockBinding
import beewhy.yanina.yumstock.di.StockComponent

class StockActivity : AppCompatActivity() {
    private lateinit var binding: ActivityStockBinding
    internal lateinit var stockComponent: StockComponent

    override fun onCreate(savedInstanceState: Bundle?) {
        stockComponent = (applicationContext as YumApplication)
            .appComponent.stockComponent().create()
        stockComponent.inject(this)

        super.onCreate(savedInstanceState)

        binding = ActivityStockBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    override fun onDestroy() {
        super.onDestroy()
//        stockComponent = null ??
    }
}