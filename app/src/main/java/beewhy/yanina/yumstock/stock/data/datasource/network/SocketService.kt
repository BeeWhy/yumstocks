package beewhy.yanina.yumstock.stock.data.datasource.network

import beewhy.yanina.yumstock.stock.data.model.StockTicker
import com.tinder.scarlet.WebSocket
import com.tinder.scarlet.ws.Receive
import com.tinder.scarlet.ws.Send
import io.reactivex.rxjava3.core.Flowable

interface SocketService {
    @Send
    fun subscribe(action: StockTicker)

    @Receive
    fun observeTicker(): Flowable<List<StockTicker>>

    @Receive
    fun observeWebSocketEvent(): Flowable<WebSocket.Event>
}