package beewhy.yanina.yumstock.stock.data.datasource.network

import beewhy.yanina.yumstock.stock.data.model.StockDetail
import io.reactivex.rxjava3.core.Observable
import retrofit2.http.GET
import retrofit2.http.Path

interface StockDetailService {

    @GET("api/stocks/{id}")
    fun getStockDetailFor(@Path("id") id: String): Observable<StockDetail>
}