package beewhy.yanina.yumstock.stock.data.model

data class SubscribeAction(
    val id: String,
    val name: String,
    val price: Double,
    val companyType: List<String>
)