package beewhy.yanina.yumstock.stock.stocklist

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import androidx.fragment.app.Fragment
import beewhy.yanina.yumstock.R
import beewhy.yanina.yumstock.databinding.FragmentStockListBinding
import beewhy.yanina.yumstock.stock.StockActivity
import com.google.android.material.snackbar.Snackbar
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

class StockListFragment : Fragment() {

    private var binding: FragmentStockListBinding? = null

    @Inject
    lateinit var viewmodel: StockListViewModel

    @Inject
    lateinit var adapter: StockListAdapter

    val compositeDisposable by lazy { CompositeDisposable() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentStockListBinding.inflate(inflater)

        binding?.listStockTickers?.adapter = adapter
        subscribeUi()
        return binding?.root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (activity as StockActivity).stockComponent.inject(this)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        compositeDisposable.dispose()
        super.onDestroy()
    }

    private fun subscribeUi() {
        compositeDisposable.add(viewmodel.getUiState()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
            when (it) {
                is StockListState.Loading -> showProgressBar()
                is StockListState.StockTickers -> {
                    hideProgressBar()
                    adapter.submitList(it.list)
                    val companyFilterAdapter = ArrayAdapter(
                        requireContext(),
                        R.layout.item_filter,
                        it.companyFilterList.toList()
                    )
                    (binding?.filterCompany?.editText as? AutoCompleteTextView)?.setAdapter(
                        companyFilterAdapter
                    )
                    val nameFilterAdapter = ArrayAdapter(
                        requireContext(),
                        R.layout.item_filter,
                        it.nameFilterList.toList()
                    )
                    (binding?.filterName?.editText as? AutoCompleteTextView)?.setAdapter(
                        nameFilterAdapter
                    )
                    Log.d(
                        "Bummer!",
                        "company filter list: " + it.companyFilterList.joinToString(",")
                    )
                    Log.d("Bummer!", "name filter list: " + it.nameFilterList.joinToString(","))

                    binding?.filterName?.editText?.addTextChangedListener(object : TextWatcher {
                        override fun afterTextChanged(s: Editable?) {
                        }

                        override fun beforeTextChanged(
                            s: CharSequence?,
                            start: Int,
                            count: Int,
                            after: Int
                        ) {
                        }

                        override fun onTextChanged(
                            s: CharSequence?,
                            start: Int,
                            before: Int,
                            count: Int
                        ) {
                            viewmodel.submitIntent(StockListIntent.FilterByName(s.toString()))
                        }
                    })

                    binding?.filterCompany?.editText?.addTextChangedListener(object : TextWatcher {
                        override fun afterTextChanged(s: Editable?) {
                        }

                        override fun beforeTextChanged(
                            s: CharSequence?,
                            start: Int,
                            count: Int,
                            after: Int
                        ) {
                        }

                        override fun onTextChanged(
                            s: CharSequence?,
                            start: Int,
                            before: Int,
                            count: Int
                        ) {
                            viewmodel.submitIntent(StockListIntent.FilterByCompany(s.toString()))
                        }
                    })
                }
                is StockListState.Error -> showError(it.error?: "Unexpected error!")
            }
        }, {
            hideProgressBar()
            showError(it.message?: "Unexpected error!")
            Log.d("Bummer!", it.message)
        }))
    }

    private fun showError(error: String) {
        binding?.let {
            Snackbar.make(it.layoutStockList, error, Snackbar.LENGTH_LONG)
                .show()
        }
    }

    private fun showProgressBar() {
        binding?.progressbar?.visibility = VISIBLE
    }

    private fun hideProgressBar() {
        binding?.progressbar?.visibility = GONE
    }
}