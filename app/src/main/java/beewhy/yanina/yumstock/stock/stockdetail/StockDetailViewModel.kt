package beewhy.yanina.yumstock.stock.stockdetail

import beewhy.yanina.yumstock.stock.data.StockRepository
import beewhy.yanina.yumstock.stock.data.model.StockDetail
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

class StockDetailViewModel @Inject constructor(val stockRepo: StockRepository) {
    fun getUiState(stockId: String): Observable<out StockDetailState> =
        stockRepo.getStockDetail(stockId).map { StockDetailState.StockDetailReady(it) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
}

sealed class StockDetailState {
    object Loading : StockDetailState()
    data class StockDetailReady(
        val stockDetail: StockDetail
    ) : StockDetailState()

    data class Error(val error: String?) : StockDetailState()
}