package beewhy.yanina.yumstock.stock.stocklist

import beewhy.yanina.yumstock.stock.data.StockRepository
import beewhy.yanina.yumstock.stock.data.model.StockTicker
import com.jakewharton.rxrelay3.BehaviorRelay
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.functions.Function3
import javax.inject.Inject

class StockListViewModel @Inject constructor(
    val filterGenerator: FilterGenerator,
    val stockRepo: StockRepository
) {

    private val companyFilter = BehaviorRelay.createDefault<String>("")
    private val nameFilter = BehaviorRelay.createDefault<String>("")

    fun submitIntent(intent: StockListIntent) {
        when (intent) {
            is StockListIntent.FilterByCompany -> updateCompanyFilter(intent.filter)
            is StockListIntent.FilterByName -> updateNameFilter(intent.filter)
        }
    }

    internal fun getUiState() =
        Observable.combineLatest(
            loadStockTickers(),
            companyFilter,
            nameFilter,
            Function3 { list, companyFilter, nameFilter ->
                val filteredList = list.filter {
                    if (filterGenerator.validateNameFilter(nameFilter, list)) {
                        it.name.startsWith(nameFilter)
                    } else true
                }.filter {
                    if (filterGenerator.validateCompanyFilter(
                            companyFilter,
                            list
                        ) && companyFilter.isNotBlank()
                    ) {
                        it.companyType.contains(companyFilter)
                    } else true
                }
                (StockListState.StockTickers(
                    filteredList,
                    filterGenerator.generateNameFilters(list),
                    filterGenerator.generateCompanyFilters(list)
                )) as StockListState
            })
            .startWith(Observable.just(StockListState.Loading))
            .onErrorReturn { StockListState.Error("Bummer!") }

    private fun loadStockTickers() = stockRepo.getStockTickers()

    private fun updateCompanyFilter(company: String) {
        companyFilter.accept(company)

    }

    private fun updateNameFilter(name: String) {
        nameFilter.accept(name)
    }
}

sealed class StockListState {
    object Loading : StockListState()
    data class StockTickers(
        val list: List<StockTicker>,
        val nameFilterList: Set<String>,
        val companyFilterList: Set<String>
    ) : StockListState()

    data class Error(val error: String?) : StockListState()
}

sealed class StockListIntent {
    class FilterByCompany(val filter: String) : StockListIntent()
    class FilterByName(val filter: String) : StockListIntent()
}