package beewhy.yanina.yumstock.stock.stocklist

import beewhy.yanina.yumstock.stock.data.model.StockTicker
import javax.inject.Inject

class FilterGenerator @Inject constructor() {

    fun generateNameFilters(list: List<StockTicker>?): Set<String> {
        val set = list?.map { it.name }?.map { it.substring(0, 2) }?.toMutableSet() ?: mutableSetOf()
        set.add("")
        return set.toSet()
    }

    fun generateCompanyFilters(list: List<StockTicker>?): Set<String> {
        val set = list?.map { it.companyType }?.flatten()?.toMutableSet() ?: mutableSetOf<String>()
        set.add("")
        return set.toSet()
    }

    fun validateNameFilter(nameFilter: String?, list: List<StockTicker>): Boolean {
        nameFilter?.let {
            return nameFilter.isNotBlank() && list.map { it.name.substring(0, 2) }
                .contains(nameFilter)
        } ?: return false
    }

    fun validateCompanyFilter(companyFilter: String?, list: List<StockTicker>): Boolean {
        companyFilter?.let {
            return companyFilter.isNotBlank() && list.map { it.companyType }.flatten()
                .contains(companyFilter)
        } ?: return false
    }
}