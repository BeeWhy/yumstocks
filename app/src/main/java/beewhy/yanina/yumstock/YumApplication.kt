package beewhy.yanina.yumstock

import android.app.Application
import beewhy.yanina.yumstock.di.ApplicationComponent
import beewhy.yanina.yumstock.di.DaggerApplicationComponent

class YumApplication : Application() {
    lateinit var appComponent : ApplicationComponent

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerApplicationComponent.factory().create(this)
    }
}